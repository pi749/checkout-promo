﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace ApplyPromo
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IService1
    {
        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }

        //columns in table dtCart: Item, Quantity, Price
        //Columns in table dtPromo: Id, Name, Price, Promotion
        public DataTable GetFinalCheckout(DataTable dtCart, DataTable dtPromo)
        {
            //add two columns (Total and Promotion Applied) to dtCart
            dtCart.Columns.Add("Total", typeof(System.Double));
            dtCart.Columns.Add("Promotion Applied", typeof(System.String));
            DataRow[] promoRows = null;
            foreach (DataRow dr in dtCart.Rows)
            {
                if (dtPromo.Select().ToList().Exists(row => row["Name"].ToString().ToUpper() == dr["Item"].ToString().ToUpper()))
                {
                    promoRows = dtPromo.Select("Name = '" + dr["Item"].ToString() + "'");
                    //if (dr["Promotion"].ToString().Equals("Buy 1 Get 1 Free"))
                    if (promoRows[0][3].ToString().Equals("Buy 1 Get 1 Free"))
                    {
                        if (Convert.ToDouble(dr["Quantity"]) > 1 && (Convert.ToDouble(dr["Quantity"]) % 2 > 0))
                        {
                            dr["Total"] = (Convert.ToDouble(dr["Price"]) * (Convert.ToDouble(dr["Quantity"]) / 2))
                                + Convert.ToDouble(dr["Price"].ToString());
                        }
                        else if (Convert.ToDouble(dr["Quantity"]) > 1 && Convert.ToDouble(dr["Quantity"]) % 2 == 0)
                        {
                            dr["Total"] = (Convert.ToDouble(dr["Price"]) * (Convert.ToDouble(dr["Quantity"]) / 2));
                        }
                        else
                            dr["Total"] = Convert.ToDouble(dr["Price"].ToString());

                        dr["Promotion Applied"] = "Buy 1 Get 1 Free";
                    }
                    else if (promoRows[0][3].ToString().Equals("3 for 10 Euro"))
                    {
                        if (Convert.ToDouble(dr["Quantity"]) > 1 && (Convert.ToDouble(dr["Quantity"]) % 3 > 0))
                        {
                            dr["Total"] = (10 * (Convert.ToDouble(dr["Quantity"]) / 3))
                                + (Convert.ToDouble(dr["Price"].ToString()) * Convert.ToDouble(dr["Quantity"]) % 3);
                        }
                        else if (Convert.ToDouble(dr["Quantity"]) > 1 && Convert.ToDouble(dr["Quantity"]) % 3 == 0)
                        {
                            dr["Total"] = (10 * (Convert.ToDouble(dr["Quantity"]) / 3));
                        }
                        else
                            dr["Total"] = Convert.ToDouble(dr["Price"].ToString());

                        dr["Promotion Applied"] = "3 for 10 Euro";
                    }
                    else
                    {
                        dr["Total"] = Convert.ToDouble(dr["Price"].ToString());
                    }

                    // add more promotions if any
                }

            }
            return dtCart;
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }
    }
}
